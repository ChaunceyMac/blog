+++
title = 'About'
date = 2024-06-01
draft = false
tags = []
+++

Hi, I'm Chauncey Mac (not my real name). I work in cybersecurity as a penetration tester. I'm very new to the field, but incredibly excited to learn and grow. Outside of work, I have a few hobbies that keep me busy:

- **Movies:** Not a film buff, but can get down with a good movie.
- **Reading:** After despising it growing up, I've recently rediscovered reading and am really enjoying it.
- **Game Development:** As a side project, I'm working on a game with a friend using Unreal Engine.
- **Electronics:** I'm interested in getting into electronics.

## Why I'm Doing This

The main goal with writing this is to keep myself accountable on my learning journey. It will also serve as a record to keep track of my progress. I don't expect people to actually read any of this, but the fact that they can will keep me honest. This is really only for myself, but if you somehow find this and get any value from it all the better.

## What You Can Expect

The only thing I want to make sure I keep a handle on is a monthly update on things that I've learned, completed, and goals I might have. The format will probably change often, and will come out on the last day of every month. 

Other than that, it's whatever I feel like writing about—maybe book or movie reviews and thoughts. I might expand upon things I've written in the monthly updates, really anything I feel like sharing.

